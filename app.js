const express = require("express")
const app = express()
const s = require("./siteconfig")
const session = require("express-session")
const sqlsession = require("connect-sqlite3")(session)
const grant = require("grant-express")

const routes = {
    index: require("./routes/index"),
    kb: require("./routes/kb"),
    search: require("./routes/search"),
    auth: require("./routes/auth"),
    sf: require("./routes/sf")
}

const isEligibleForSuggestions = () => { 
    if (
        s.enable.suggestions &&
        s.cookieSecret &&
        s.auth.appId &&
        s.auth.appSecret
    ) return true
    else return false
}

if (s.enable.suggestions && !isEligibleForSuggestions()) {
    console.log("Suggestions not enabled due to misconfiguration. Please make sure your configuration is correct.")
}

app.use(session({
    secret: s.cookieSecret,
    store: new sqlsession,
    resave: false,
    saveUninitialized: false
}))
app.use(grant({
    "defaults": {
        "protocol": "http",
        "transport": "session",
        "state": true
    },
    "github": {
        "key": s.auth.appId,
        "secret": s.auth.appSecret,
        "callback": "/auth/github/callback"
    }
}))

app.set("view engine", "ejs")

app.use(express.json())
app.use(express.static("pub"))

app.use("/", routes.index)
app.use("/kb", routes.kb)
app.use("/search", routes.search)
app.use("/auth", routes.auth)
app.use("/sf", routes.sf)

app.use((req, res) => {
    res.render("index/pnf", {
        cfg: s
    })
})

app.listen(8080, () => {
    console.log("http://localhost:8080")
})