const searchbarNav = document.querySelector("#navsearchbar")
const searchbtnNav = document.querySelector("#navsearchbtn")

const homepageBar = document.querySelector("#homesearchbar") || document.createElement("input")
const homepageBtn = document.querySelector("#homesearchbtn") || document.createElement("button")

homepageBtn.addEventListener("click", () => {
    document.location.href = "/search/results?q=" + encodeURIComponent(homepageBar.value)
})

searchbtnNav.addEventListener("click", () => {
    document.location.href = "/search/results?q=" + encodeURIComponent(searchbarNav.value)
})