Congratulations, HMDesk is up & running.  
  
### What next?  
  
  * Modify the config file to your likings in `siteconfig.js`.  
    * The `cookieSecret` property will be used to secure session cookies, generate a cryptographically secure password if you must!
  * Write some articles and enjoy.