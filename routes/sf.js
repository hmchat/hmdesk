const express = require("express")
const router = express.Router()

const s = require("../siteconfig")
const app = require("../app")

router.use((req, res, next) => {
    if (s.enable.suggestions) next()
    else return res.render("kb/disabled", { cfg: s })
})

router.use((req, res, next) => {
    if (
        s.enable.suggestions &&
        s.cookieSecret &&
        s.auth.appId &&
        s.auth.appSecret
    ) next()
    else return res.render("sf/notEligible", { cfg: s })
})

router.get("/", (req, res) => {
    res.render("sf/index", { cfg: s })
})

router.get("/new", (req, res) => {
    if (req.session.ghUser) {
        res.render("sf/new", { cfg: s, user: req.session.ghUser })
    } else {
        res.redirect("/connect/github")
    }
})

module.exports = router