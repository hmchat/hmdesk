const express = require("express")
const router = express.Router()

const s = require("../siteconfig")

const fs = require("fs")
const path = require("path")

const hljs = require("highlight.js")
const mdCon = require("markdown-it")({
    highlight: (str, lang) => {
        if (lang && hljs.getLanguage(lang)) {
            try {
                return hljs.highlight(lang, str).value;
            } catch (__) { }
        }

        return ''; // use external default escaping
    },
    html: true
})


router.use((req, res, next) => {
    if (s.enable.kb) next()
    else return res.render("kb/disabled", { cfg: s })
})

router.get("/", (req, res) => {
    let articles = []
    const categories = fs.readdirSync(path.join(__dirname, "..", "kb", "entries"))
    categories.forEach(cat => {
        const subcats = fs.readdirSync(path.join(__dirname, "..", "kb", "entries", cat))
        articles.push({ name: cat, subarticles: subcats })
    })

    res.render("kb/index", {
        cfg: s,
        articles: articles
    })
})

router.get("/article/:cat/:subcat", (req, res) => {
    const cats = fs.readdirSync(path.join(__dirname, "..", "kb", "entries"))
    if (!cats.includes(req.params.cat)) return res.render("index/pnf", { cfg: s })
    const subcats = fs.readdirSync(path.join(__dirname, "..", "kb", "entries", req.params.cat))
    if (!subcats.includes(req.params.subcat + ".md")) return res.render("index/pnf", { cfg: s })

    const articleBuffer = fs.readFileSync(path.join(__dirname, "..", "kb", "entries", req.params.cat, req.params.subcat + ".md"))
    const articleMarkdown = articleBuffer.toString()
    const article = mdCon.render(articleMarkdown)
    
    res.render("kb/article", {
        cfg: s,
        article: {
            cat: req.params.cat,
            name: req.params.subcat,
            body: article
        }
    })
})

router.get("/article/:cat", (req, res) => {
    const cats = fs.readdirSync(path.join(__dirname, "..", "kb", "entries"))
    if (!cats.includes(req.params.cat)) return res.render("index/pnf", { cfg: s })
    const subcats = fs.readdirSync(path.join(__dirname, "..", "kb", "entries", req.params.cat))

    res.render("kb/category", {
        cfg: s,
        cat: {
            name: req.params.cat,
            contents: subcats
        }
    })
})

module.exports = router