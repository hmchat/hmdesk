const express = require("express")
const router = express.Router()

const s = require("../siteconfig")

const fs = require("fs")
const path = require("path")

router.get("/results", (req, res) => {
    const q = req.query
    if (!q.q) return res.render("index/pnf", {
        cfg: s
    })

    let articles = []
    const categories = fs.readdirSync(path.join(__dirname, "..", "kb", "entries"))
    categories.forEach(cat => {
        const subcats = fs.readdirSync(path.join(__dirname, "..", "kb", "entries", cat))
        subcats.forEach(subcat => {
            articles.push({ name: subcat, article: cat })
        })
    })

    res.render("search/results", {
        results: articles.filter(v => v.name.toLowerCase().includes(q.q.toLowerCase())),
        cfg: s
    })
})

module.exports = router