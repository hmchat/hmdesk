const express = require("express")
const router = express.Router()

const s = require("../siteconfig")

router.get("/", (req,res) => {
    res.render("index/index", {
        cfg: s
    })
})

module.exports = router