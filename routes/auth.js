const express = require("express")
const router = express.Router()
const fetch = require("node-fetch")

const s = require("../siteconfig")

router.get("/github/callback", (req, res) => {
    if (req.session.grant) {
        fetch("https://api.github.com/user", {
            headers: {
                Accept: "application/vnd.github.v3+json",
                Authorization: `token ${req.session.grant.response.access_token}`
            }
        }).then(ghRes => ghRes.json().then(body => {
            req.session.ghUser = body.login
            req.session.save()
            res.redirect("/sf")
        }))
    } else {
        res.send('Unauthorized. <a href="/connect/github">Authorize</a>')
    }
})

module.exports = router